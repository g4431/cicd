# CI DI

[link to the course](https://www.udemy.com/course/gitlab-cicd-course/learn/lecture/28109888?start=0#overview)

## definition
### CI:
 Continious integration: requires the developers to integrate their code  into a central and shared code repository such as Git 

### CD: 
- Countinous Delivery: deploy an automated release process, aims at building testing and releaseing software with greater speed  (require approval and intervention of humain )
- Countinous Deployment: software engineering approach in which features are delivered frequently through automated deployment systems (without explicit approval)

### CI/CD: 
set of principales that need to be adopted to automate and speed the development and the deployment cycle

### SDLC (software development lifecycle)
systematic process used by software industry for building the high quality software 
![SDLC](images/sdlc.png)


## Gitlab
web based complete devOps platform , is not a monolithic app

### Pipeline
top level component of continous integration, delivery and deployment, comprise two things "jobs" and "stages":
- Job : defines what to do, job to test the code, or compile the code
- stage: collection of job, it defines the order of jobs 

if all jobs in stage succeed the pipeline moves to the next stage 
Gitlab allows you to manually interect with a pipeline.

The usual stages of a CI/CD pipeline are test,build, stage and prod, by default test stage

.gitlab-ci.yml: configuration of CICD gitlab 

### variables

- predefined variables
- custom variables

### gitlab registry

free to use for docker images by following the convention:
registry URL(registry.gitlab.com)/ namespace(gitlab username or group name) / project / image 
registry URL(registry.gitlab.com)/ namespace(gitlab username) / project : $CI_REGISTRY_IMAGE

## CD Heroku

heroku registry naming: registry.heroku.com/ app(in heroku) / process-type 

## Deployments strategies
- Recreate Deployment: the older version is shut down and the newer app version is scaled up to audience
    - pros: simple and fast, you ll not spend extra infra costs
    - cons: riskiest deployment, downtime during the update process
- Blue Green (red / black deployment): you perform two identical deployments of your app, the blue represents the current stable version of app and the green represents the new app
    - pros: zero down time, instant rollback
    - cons: increase operational overhead and cost
- Canary deployment: it is blue/green strategy that is more risk averse, collect and analyse the important metrics and feedbacks to decide wether the new update is good for a full scale rollout
    - pros: lowest risk prone, because of version release control, zerodown time
    - cons: scripting canary release is complexe, slow rollout
- A/B testing: similary to canary deployment however A/B testing relies on real world statistical data to decide on a rollout or rollback
    - pros: best choice for real world statistic data
    - cons: breaks UX, scripting is complexe